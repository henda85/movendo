import React from "react";
import { useState } from "react";
import Image from "next/image";
import Link from "next/link";

function Nav() {
  const [showLinks, setShowLinks] = useState(false);

  const handleShowLinks = () => {
    setShowLinks(!showLinks);
  };
  return (
    <nav className={`navbar ${showLinks ? "show-nav" : "hide-nav"} `}>
      <div className="navbar_logo">
        <Link href="/">
          <Image
            src="/logo.png"
            height={80}
            width={100}
            className="cursor-pointer  bg-white rounded-full center"
            alt="movendo"
          />
        </Link>
      </div>
      <ul className="navbar_links">
        <Link href="/">
          <a className="navbar_link">
            <li className="navbar_items">Accueil</li>
          </a>
        </Link>
        <Link href="/trousse">
          <a className="navbar_link">
            <li className="navbar_items">Trousse de secours mentale</li>
          </a>
        </Link>
        <Link href="/formation">
          <a className="navbar_link">
            <li className="navbar_items">Formation</li>
          </a>
        </Link>
        <Link href="/apropos">
          <a className="navbar_link">
            <li className="navbar_items">A propos</li>
          </a>
        </Link>
        <Link href="/temoignages">
          <a className="navbar_link">
            <li className="navbar_items">Témoignages</li>
          </a>
        </Link>
        <Link href="/contact">
          <a className="navbar_link">
            <li className="navbar_items">Contact</li>
          </a>
        </Link>
      </ul>
      <button className="navbar-burger" onClick={handleShowLinks}>
        <span className="burger-bar"></span>
      </button>
    </nav>
  );
}

export default Nav;
