import React from "react";
import Image from "next/image";
import { ChevronRightIcon } from "@heroicons/react/solid";
import Link from "next/link";
function Footer() {
  return (
    <div className="bg-[#008a8c] flex flex-col items-start  lg:flex-row px-8 pb-4   ">
      <div className="lg:w-1/3">
        <Link href="/">
          <Image
            src="/logo.png"
            height={150}
            width={150}
            className="cursor-pointer"
            alt="movendo"
          />
        </Link>
      </div>

      <div className="lg:w-1/3">
        <p className="text-2xl mb-2 mt-4 ">Contact</p>
        <a href="mailto:contact@example.com">
          <div className="flex">
            <Image
              src="/icons8-email-64.png"
              height={30}
              width={30}
              className="cursor-pointer"
              alt="email"
            />
            <p>contact@example.com</p>
          </div>
        </a>
        <a href="tel:+33500000000">
          <div className="flex">
            <Image
              src="/icons8-téléphone-52.png"
              height={30}
              width={30}
              className=""
              alt="télépnone"
            />
            <p>+33500000000</p>
          </div>
        </a>
      </div>
      <div className="lg:w-1/3">
        <p className="text-2xl mt-4 mb-2 ">Nous suivre</p>
        <a href="">
          <Image
            src="/instagram.png"
            height={30}
            width={30}
            className=""
            alt="instagram"
          />
        </a>
        <a href="">
          <Image
            src="/linkedin.png"
            height={30}
            width={30}
            className=""
            alt="linkedin"
          />
        </a>
      </div>
    </div>
  );
}

export default Footer;
