import React from "react";
import Image from "next/image";
function Accueil1() {
  return (
    <div className="bg-imgHome bg-center bg-cover p-28 lg:p-52">
      <Image
        src="/logo.png"
        height={200}
        width={200}
        className="relative h-32 z-0"
        alt="movendo"
      />
      <div className="mb-0 py-0">
        <p className="font-semibold text-xl font-bold  md:text-4xl">
          {`Former l'entreprise à la santé`}
        </p>
      </div>
    </div>
  );
}

export default Accueil1;
