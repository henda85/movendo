import React from "react";

import { ChevronRightIcon } from "@heroicons/react/solid";
("@heroicons/react/solid");
function Accueil2() {
  return (
    <div className="lg:flex mb-4">
      <div
        className="bg-[#f9b347] ml-3 mr-3 mt-8 mb-1 text-center py-3 shadow-2xl lg:mb-0
      lg:w-2/3 lg:mr-1 lg:text-left lg:pl-6"
      >
        <h2 className="text-2xl py-3 ">Notre mission?</h2>
        <p className="text-sm  ml-4 mr-4 lg:text-xl lg:ml-0  ">
          Accompagner le dirigeant ainsi que ses collaborateurs à être plus
          sereins, en leur apportant des outils favorisant le bien-être
          physique, mental et social.
        </p>
        <div className="flex items-center justify-center text-center py-4 lg:justify-start lg:pl-1">
          <p className="py-2 text-xl cursor-pointer">Qui sommes nous ?</p>
          <ChevronRightIcon className="h-6 cursor-pointer" />
        </div>
        <div className="flex items-center justify-center text-center lg:justify-start lg:pl-1">
          <p className="py-2 text-xl mb-2 cursor-pointer ">
            Nos outils et méthodes
          </p>
          <ChevronRightIcon className="h-6 cursor-pointer" />
        </div>
      </div>
      <div
        className="bg-[#008a8c] ml-3 mr-3  text-center py-3 
      lg:w-1/3 lg:mt-8 lg:ml-1 lg:text-left lg:pl-6"
      >
        <h2 className="text-2xl py-3 px-3">
          {`Notre programme d'accompagnement est fait pour vous !`}
        </h2>
        <div className="flex items-center justify-center text-center lg:justify-start lg:pl-4">
          <p className="py-2 text-xl cursor-pointer ">Pour les dirigeants </p>
          <ChevronRightIcon className="h-6 cursor-pointer  " />
        </div>

        <div className="flex items-center justify-center text-center lg:justify-start lg:pl-4">
          <p className="py-2 text-xl cursor-pointer ">Pour les entreprises</p>
          <ChevronRightIcon className="h-6 cursor-pointer" />
        </div>
      </div>
    </div>
  );
}

export default Accueil2;
