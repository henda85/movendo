import {
  AcademicCapIcon,
  PresentationChartBarIcon,
  PresentationChartLineIcon,
} from "@heroicons/react/solid";
import React from "react";

function Mission() {
  return (
    <div className="">
      <div className="flex">
        <div className=" flex flex-col text-center py-6 lg:w-1/3 lg:mx-96">
          <h2 className="text-[#008a8c] text-2xl font-bold">
            {" Développons le bien-être mental de vos étudiants "}
          </h2>
          <PresentationChartLineIcon className="h-28 text-[#008a8c] " />
          <h3 className="py-2 text-[#f9b347] text-2xl">{"L'AUDIT"}</h3>
          <p className="px-4">
            {`Notre audit est un questionnaire breveté qui nous permettra de
            mesurer votre état d'être et/ou celui de vos collaborateurs. Cet
            audit nous permettra de mettre en place un plan d'action ayant pour
            but d'améliorer la qualité de vie dans toutes les composantes de
            votre environnement professionnel.`}
          </p>
        </div>
      </div>
      <div className="lg:flex lg:items-center">
        <div className=" flex flex-col text-center py-6 lg:w-1/3 lg:mr-48">
          <AcademicCapIcon className="h-28 text-[#008a8c]" />
          <h3 className="py-2 text-[#f9b347] text-2xl">LES FORMATIONS</h3>
          <p className="px-4">
            {`Notre équipe de formateurs propose un large choix de formations dans
            les domaines de la santé préventive et du bien-être au travail. En
            suivant le plan d'action défini, nous créerons votre programme
            personnalisé en sélectionnant les formations adaptées à vos besoins
            et à ceux de votre entreprise.`}
          </p>
        </div>
        <div className=" flex flex-col text-center py-6  lg:w-1/3 lg:ml-48">
          <div className="flex flex-col jutify-center items-center">
            <img
              src="/human.png"
              className="text-[#008a8c] center h-20 w-40 "
              alt="L'ÉVALUATION"
            />
          </div>
          <h3 className="py-2 text-[#f9b347] text-2xl">{"L'ÉVALUATION"}</h3>
          <p className="px-4">
            {`En fin de programme nous faisons le point et analysons ensemble les
            bénéfices obtenus. Pour aller plus loin, vos collaborateurs pourront
            s'ils le souhaitent, continuer individuellement leur accompagnement.`}
          </p>
        </div>
      </div>
      <div className="flex">
        <div className=" flex flex-col text-center py-6 lg:w-1/3 lg:mx-96">
          <div className="flex flex-col jutify-center items-center">
            <img
              src="/hand3.png"
              className=" center h-20 w-40 "
              alt="ACCOMPAGNEMENT"
            />
          </div>

          <h3 className="py-2 text-[#f9b347] text-2xl">{`L' ACCOMPAGNEMENT`}</h3>
          <p className="px-4 ">
            {`Au moment du déploiement de la solution nous vous accompagnons par
            le biais d'une communication interne adaptée à votre structure. Tout
            au long de votre programme, nous vous accompagnons et mesurons en
            temps réel les évolutions et résultats obtenus.`}
          </p>
        </div>
      </div>
    </div>
  );
}

export default Mission;
