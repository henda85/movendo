import React from "react";

function ChifresClés() {
    return (
        <div>
        <div className="">
            <div className="flex justify-center items-center">
                <img src="/yes.jpg" className="w-3/4 h-48" alt="chifre-clés" />
            </div>
            
            <h2 className="text-[#008a8c] text-2xl text-center mt-4 font-bold">
            Chifres clés
            </h2>
            <div className="ml-10 mr-5">
            <h3 className="text-2xl text-[#f9b347] py-10 font-bold">2020 en chifres</h3>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, ut.
                Asperiores hic ratione perferendis? Aliquid excepturi architecto,
                quis deleniti magni illum provident quisquam accusamus et sint,
                consequuntur fugiat quod soluta. Lorem ipsum dolor sit amet
                consectetur adipisicing elit. Blanditiis, aperiam ratione. Esse,
                harum eos laboriosam molestias consequuntur enim sit provident.
                Tenetur aperiam rem magnam quos quod consectetur nemo qui nesciunt!
            </p>
            <h3 className="text-2xl text-[#f9b347] py-4 font-bold">Titre</h3>
            <div className="lg:grid lg:grid-cols-2">
                <div className="py-4">
                    <p className="text-xl text-blue-500 font-bold">petit titre %</p>
                    <p>lorem ipsum dolor</p>
                </div>
                <div className="py-6">
                    <p className="text-xl text-blue-500 font-bold">petit titre %</p>
                    <p>lorem ipsum dolor</p>
                </div>
                <div className="py-6">
                    <p className="text-xl text-blue-500 font-bold">petit titre M€</p>
                    <p>lorem ipsum dolor</p>
                </div>
                <div className="py-6">
                    <p className="text-xl text-blue-500 font-bold">petit titre %</p>
                    <p>lorem ipsum dolor</p>
                </div>
            </div>
            <h3 className="text-2xl text-[#f9b347] py-4 font-bold">Titre</h3>
            <div className="lg:grid lg:grid-cols-2">
                <div className="py-6">
                
                    <p className="text-xl text-gray-500 font-bold">petit titre %</p>
                    <p>lorem ipsum dolor</p>
                </div>
                <div className="py-6">
                <p className="text-xl text-gray-500 font-bold">petit titre %</p>
                <p>lorem ipsum dolor</p>
                </div>
                <div className="py-6">
                <p className="text-xl text-gray-500 font-bold">petit titre %</p>
                <p>lorem ipsum dolor</p>
                </div>
                <div className="py-6">
                <p className="text-xl text-gray-500 font-bold">petit titre %</p>
                <p>lorem ipsum dolor</p>
                </div>
            </div>
            </div>
        </div>
        </div>
    );
    }
                

    export default ChifresClés;
                
                
