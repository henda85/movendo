import Head from "next/head";
import React from "react";

function formdevis() {
  return (
    <div>
      <Head>
        <title>Devis</title>

        <link rel="icon" href="/favicon.ico" />
      </Head>
      <form className="  lg:w-3/5">
        <div className="flex flex-col  space-y-2   ">
          <input
            type="text"
            className="border-2 border-[#B45309] rounded-md  placeholder-gray-500 "
            placeholder="nom"
          />
          <input
            type="email"
            placeholder="exemple@email.com"
            className="border-2 border-[#B45309] rounded-md placeholder-gray-500 "
          />
          <input
            type="textarea"
            row="3"
            className="border-2 border-[#B45309] rounded-md placeholder-gray-500"
          />

          <textarea
            className="border-2 border-[#B45309] rounded-md placeholder-gray-500"
            name="commentaires"
            placeholder="Tapez ici vos commentaires"
          ></textarea>
          <button type="submit" className="bg-[#008a8c] rounded-full">
            Envoyer
          </button>
        </div>
      </form>
    </div>
  );
}

export default formdevis;
