module.exports = {
  mode:'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: {

        'imgHome':"url('/image2.jpg')",
        'imgApropos':"url('/apropos.jpg')",
        'imgcontact':"url('/contact.jpg')",
        
       }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
